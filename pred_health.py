import numpy as np
import pandas as pd
from datetime import datetime

def read_file(path):
    subject_data = {}
    with pd.HDFStore(path) as hdfs:
        subject_data.update({'Acceleration':np.array(hdfs['Acceleration'].value)})
        subject_data.update({'Acceleration_Index':np.array(hdfs['Acceleration'].index)})
        subject_data.update({'Heart_Rate':np.array(hdfs['Heart_Rate']['value'])})
        subject_data.update({'Heart_Rate_Confidence':np.array(hdfs['Heart_Rate']['conf'])})
        subject_data.update({'Heart_Rate_Index':np.array(hdfs['Heart_Rate'].index)})
        subject_data.update({'Breathing_Rate':np.array(hdfs['Breathing_Rate']['value'])})
        subject_data.update({'Breathing_Rate_Confidence':np.array(hdfs['Breathing_Rate']['conf'])})
        subject_data.update({'Breathing_Rate_Index':np.array(hdfs['Breathing_Rate'].index)})
        subject_data.update({'RR_Interval':np.array(hdfs['RR_Interval']['value'])})
        subject_data.update({'RR_Interval_Confidence':np.array(hdfs['RR_Interval']['conf'])})
        subject_data.update({'RR_Interval_Index':np.array(hdfs['RR_Interval'].index)})
        subject_data.update({'Sleep_Stages':np.array(hdfs['Sleep_Stages'].value)})
        subject_data.update({'Sleep_Stages_Index':np.array(hdfs['Sleep_Stages'].index)})
        subject_data.update({'Profile_Age':hdfs['Profile']['age'].tolist()[0]})
        subject_data.update({'Profile_Gender':hdfs['Profile']['gender'].tolist()[0]})
        subject_data.update({'Profile_Weight':hdfs['Profile']['weight'].tolist()[0]})
        subject_data.update({'Profile_Height':hdfs['Profile']['height'].tolist()[0]})
        subject_data.update({'Profile_Blood_Pressure_Systolic':hdfs['Profile']['blood_pressure_systolic'].tolist()[0]})
        subject_data.update({'Profile_Blood_Pressure_Diastolic':hdfs['Profile']['blood_pressure_diastolic'].tolist()[0]})
        subject_data.update({'Profile_Cholesterol':hdfs['Profile']['cholesterol'].tolist()[0]})
        subject_data.update({'Profile_Smoker':hdfs['Profile']['smoker'].tolist()[0]})
        
    return(subject_data)

def pred_health(subject):
    health_score=100
    Problem_Areas=[]
    
    #assessing BMI
    if subject.BMI<17.5:
        health_score-=5
        Problem_Areas.append('Underweight')
    if subject.BMI>=30:
        health_score-=10
        Problem_Areas.append('Obese')
    
    #Assessing Blood Preassure
    if subject.Profile_Blood_Pressure_Diastolic<80: 
        if 120<=subject.Profile_Blood_Pressure_Systolic<=129:
            health_score-=5
            Problem_Areas.append('Elevated Blood Pressure')
    elif (80<=subject.Profile_Blood_Pressure_Diastolic<=89 or 
          130<=subject.Profile_Blood_Pressure_Systolic<=139):
            health_score-=10
            Problem_Areas.append('Blood Pressure - Hypertension Stage 1')
    elif (90<=subject.Profile_Blood_Pressure_Diastolic or 
          140<=subject.Profile_Blood_Pressure_Systolic):
        if (120<=subject.Profile_Blood_Pressure_Diastolic or 
            180<=subject.Profile_Blood_Pressure_Systolic):
            health_score-=50
            Problem_Areas.append('Blood pressure - Hypertensive Crisis')
        else:
            health_score-=15
            Problem_Areas.append('Blood Pressure - Hypertension Stage 2')
            
    # Assessing Cholesterol
    if 5.2<=subject.Profile_Cholesterol<=6.2:
        health_score-=5
        Problem_Areas.append('Cholesterol - Borderline high')
    elif subject.Profile_Cholesterol>6.2:
        health_score-=10
        Problem_Areas.append('Cholesterol - High')
    
    # Assessing Smoking
    if subject.Profile_Smoker=='Yes':
        health_score-=10
        Problem_Areas.append('Smoking')
    
    # Assessing Sleep
    if subject.Average_Hours<6 or subject.Average_Hours>8:
        health_score-=5
        Problem_Areas.append('Average Sleep not in recommended range')
    if (subject.Average_Rem<0.20 or subject.Average_Hours>0.25 or 
        subject.Average_Deep<0.13 or subject.Average_Hours>0.23):
        health_score-=5
        Problem_Areas.append('Percentage Rem/Deep Sleep not in recommended range')
    
    # Resting Heart Rate
    if subject.Resting_HR>=75:
        health_score-=15
        Problem_Areas.append('High Resting Heart Rate')
    
    # Assessing Breating Rate
    
    if subject.Breathing_Rate<8:
        health_score-=50      
        Problem_Areas.append('Very High Risk - Breathing rate too low')  
    elif subject.Breathing_Rate>28:
        health_score-=10      
        Problem_Areas.append('Breating Rate not in recommended range')  
    elif subject.Breathing_Rate<12 or subject.Breathing_Rate>20:
        health_score-=5      
        Problem_Areas.append('Breating Rate not in recommended range')  
    
    if health_score<0:
        return 0,Problem_Areas
    else:
        return health_score,Problem_Areas

def make_prediction(file_path):
    # Summary Data
    Profile_Info={'Profile_Gender':[],'Profile_Blood_Pressure_Diastolic':[],'Profile_Cholesterol':[],
                  'Profile_Blood_Pressure_Systolic':[],'Profile_Height':[],'Profile_Weight':[],
                  'Profile_Age':[],'Profile_Smoker':[],'BMI':[]}

    subject_data=read_file(file_path)
    Profile_Info['Profile_Gender'].append(subject_data['Profile_Gender'])
    Profile_Info['Profile_Blood_Pressure_Diastolic'].append(subject_data['Profile_Blood_Pressure_Diastolic'])
    Profile_Info['Profile_Blood_Pressure_Systolic'].append(subject_data['Profile_Blood_Pressure_Systolic'])
    Profile_Info['Profile_Cholesterol'].append(subject_data['Profile_Cholesterol'])
    Profile_Info['Profile_Height'].append(subject_data['Profile_Height'])
    Profile_Info['Profile_Weight'].append(subject_data['Profile_Weight'])
    Profile_Info['Profile_Age'].append(subject_data['Profile_Age'])
    Profile_Info['Profile_Smoker'].append(subject_data['Profile_Smoker'])
    Profile_Info['BMI'].append(10000*subject_data['Profile_Weight']/subject_data['Profile_Height']**2)

    Profile_DF=pd.DataFrame.from_dict(Profile_Info)

    # Sleep Data
    def minsdiff(x,y):
        return (x-y)/6e+10

    sleep_data={'Average_Hours':[],'Average_Rem':[],'Average_Deep':[]}
    # subject_data=all_subject_data['subject{}'.format(j)]
    ss_dict={}
    ss_dict['Stage']=subject_data['Sleep_Stages']
    ss_dict['Index']=subject_data['Sleep_Stages_Index']
    ss_df=pd.DataFrame.from_dict(ss_dict)
    ss_df.loc[ss_df.Stage=='sleep_wake_stop','tmp_sleep_stages']=0
    ss_df.loc[ss_df.Stage=='sleep_rem_stop','tmp_sleep_stages']=1
    ss_df.loc[ss_df.Stage=='sleep_light_stop','tmp_sleep_stages']=2
    ss_df.loc[ss_df.Stage=='sleep_deep_stop','tmp_sleep_stages']=3
    ss_df_new=ss_df.loc[~ss_df.tmp_sleep_stages.isna()].reset_index()

    for i in range(1,ss_df_new.shape[0]):
        ss_df_new.loc[ss_df_new.index==i,'MinsDiff']=minsdiff(list(ss_df_new.Index)[i],list(ss_df_new.Index)[i-1])

    sleep_stage_df=ss_df_new.loc[ss_df_new.MinsDiff!=0].groupby('tmp_sleep_stages').agg({'MinsDiff':sum}).reset_index()
    sleep_stage_df['Hours_Per_Day']=sleep_stage_df.MinsDiff.apply(lambda x:(x/60)/7)
    Average_Hours=sum(sleep_stage_df.loc[sleep_stage_df.tmp_sleep_stages!=0].Hours_Per_Day)
    Average_Rem=sum(sleep_stage_df.loc[sleep_stage_df.tmp_sleep_stages==1].Hours_Per_Day)/Average_Hours
    Average_Deep=sum(sleep_stage_df.loc[sleep_stage_df.tmp_sleep_stages==3].Hours_Per_Day)/Average_Hours
    sleep_data['Average_Hours'].append(Average_Hours)
    sleep_data['Average_Rem'].append(Average_Rem)
    sleep_data['Average_Deep'].append(Average_Deep)

    sleep_data_df=pd.DataFrame.from_dict(sleep_data)

    # Resting Heart rate

    hr=[]
    hr_dict={}
    hr_dict['Rate']=subject_data['Heart_Rate']
    hr_dict['Index']=subject_data['Heart_Rate_Index']
    hr_dict['Confidence']=subject_data['Heart_Rate_Confidence']
    hr_df=pd.DataFrame.from_dict(hr_dict)
    exercise_hr=max(hr_df.Rate)*0.55
    hr_df_ar=hr_df.loc[(hr_df.Rate<exercise_hr)&(hr_df.Confidence>=50)]
    hr.append(np.mean(hr_df_ar.Rate))

    Resting_HR=pd.DataFrame(data=hr,columns=['Resting_HR'])

    # Breathing Rate

    br=[]
    br_dict={}
    br_dict['Rate']=subject_data['Breathing_Rate']
    br_dict['Index']=subject_data['Breathing_Rate_Index']
    br_dict['Confidence']=subject_data['Breathing_Rate_Confidence']
    br_df=pd.DataFrame.from_dict(br_dict)
    br_df_ar=br_df.loc[br_df.Confidence>=50]
    br.append(np.mean(br_df_ar['Rate']))

    Breathing_Rate=pd.DataFrame(data=br,columns=['Breathing_Rate'])

    # Combine All Data
    Profile_DF=Profile_DF.join(sleep_data_df, how='inner')
    Profile_DF=Profile_DF.join(Resting_HR, how='inner')
    Profile_DF=Profile_DF.join(Breathing_Rate, how='inner')

    return pred_health(Profile_DF.iloc[0])

print make_prediction('./data/subject_8.h5')